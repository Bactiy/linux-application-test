/**创建一个有名管道
 * 以写的方式打开这个有名管道，并写入一个值
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>

int main(void)
{
    int ret, fd;
    char p_flag = 0;
    /* 创建一个有名管道 */
    if (access("./fifo", 0) < 0) //判断如果有名管道是否存在，不存在的话就创建
    {
        ret = mkfifo("./fifo", 0777); //创建有名管道
        if (ret < 0)
        {
            printf("creat name pipe fail\n");
            return -1;
            /* code */
        }
        printf("creat name pipe sucess\n");

        /* code */
    }
    /* 打开有名管道-以写的方式打开*/
    fd = open("./fifo", O_WRONLY);
    if (fd < 0)
    {
        printf("open fifo fail\n");
        return -1;
        /* code */
    }
    printf("open fifo sucess\n");
    for (int i = 0; i < 5; i++)
    {
        printf("this is parent %d\n", i);
        usleep(100);
        /* code */
    }
    p_flag = 1;
    write(fd, &p_flag, sizeof(p_flag));
    while (1)
        ;
    return 0;
}
