/**
 * 父子进程通过无名管道通信
 * 父进程向管道写入一个值
 * 子进程从管道读取，如果非零，则执行后面的程序
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>

int main(void)
{
    pid_t pid;
    int fd[2];
    int ret = 0;
    int flag = 0;

    //创建一个无名管道
    ret = pipe(fd);
    if (ret < 0)
    {
        printf("create pipe fail\n");
        return -1;
        /* code */
    }
    printf("creat pipe sucess\n");

    pid = fork(); //在父进程中创建一个子进程

    if (pid == 0)
    {
        read(fd[0], &flag, sizeof(flag));
        while (flag == 0)
            ;
        for (int i = 0; i < 5; i++)
        {
            usleep(100);
            printf("this is child %d\n", i);
            /* code */
        }
        /* code */
    }
    if (pid > 0)
    {
        for (int i = 0; i < 5; i++)
        {
            usleep(100);
            printf("this is parent %d\n", i);
            /* code */
        }
        flag = 1;
        sleep(2);
        write(fd[1], &flag, sizeof(flag));
        /* code */
    }
    while (1);
    return 0;
}
