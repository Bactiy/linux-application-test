/**创建一个管道，往管道写字符串，并从管道读取
 * 
 * 
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(void)
{

    int fd[2];
    int ret = 0;
    char write_buf[] = "hello linux";
    char read_buf[123] = {};
    ret = pipe(fd);
    if (ret<0)
    {
        printf("creat pipe fail\n");
        return -1;
        /* code */
    }
    printf("creat pipe sucess fd[0] = %d,fd[1] = %d\n",fd[0],fd[1]);
    // 向文件描述符fd[1]写管道
    write(fd[1],write_buf,sizeof(write_buf));
    // 从文件描述符fd[0]读管道
    read(fd[0],read_buf,sizeof(read_buf));

    printf("read_buf=%s\n",read_buf);
    close(fd[0]);
    close(fd[1]);
    
    return 0;
}