/**
 * 以读的方式打开这个有名管道，如果读取到flag有值就执行后面的程序
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>

int main(void)
{
    int ret, fd;
    fd = open("./fifo", O_RDONLY);
    char p_flag = 0;
    /* 打开有名管道-以写的方式打卡*/
    
    if (fd < 0)
    {
        printf("open fifo fail\n");
        return -1;
        /* code */
    }
    printf("open fifo sucess\n");
    read(fd,&p_flag,sizeof(p_flag));
    while(!p_flag);
    for (int i = 0; i < 5; i++)
    {
        printf("this is child %d\n", i);
        usleep(100);
        /* code */
    }
    while (1)
        ;
    return 0;
}


