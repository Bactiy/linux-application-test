/**1.在父进程创建使用key值为IPC_PRIVATE创建一个共享内存
 * 2.在父进程中创建一个子进程
 * 3.通过输入，父进程向共享内存中写入字符串
 * 4.父进程调用发送信号函数通知子进程可以读取共享内存数据
 * 5.子进程收到父进程发送过来的信号，开始读取共享内存数据
 * 6.子进程读完共享内存数据后，发送信号通知父进程读取完成
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <signal.h>

void myfunc(int signum)
{
    switch (signum)
    {
    case SIGUSR2:
        printf("SIGUSR2\n");
        /* code */
        break;
    case SIGUSR1:
        printf("SIGUSR1\n");
        /* code */
        break;
    default:
        break;
    }
}

int main(void)
{
    int shmid;
    int key;
    char *p;
    int pid;

    shmid = shmget(IPC_PRIVATE, 128, IPC_CREAT | 0777);
    if (shmid < 0)
    {
        printf("creat share memory fail\n");
        return -1;
        /* code */
    }
    printf("creat share memory sucess,shmid=%d\n", shmid);
    pid = fork(); // 新建一个进程
    printf("\nfork_pid:%d\n", pid);
    if (pid > 0) //父进程
    {
        signal(SIGUSR2, myfunc);           //设置一个函数来处理信号
        p = (char *)shmat(shmid, NULL, 0); //用来允许本进程访问一块共享内存的函数
        if (p == NULL)
        {
            printf("shmat fail\n");
            return -1;
            /* code */
        }
        printf("parent process shmat sucess!\n");
        while (1)
        {
            printf("P1pid:%d\n", pid);
            printf("\nparent process begin input:");
            fgets(p, 128, stdin);
            kill(pid, SIGUSR1); //  发送信号通知子进程读取共享内存
            printf("P2pid:%d\n", pid);
            pause();
            /* code */
        }
        /* code */
    }
    if (pid == 0)
    { // 子进程
        printf("C1pid:%d\n", pid);
        signal(SIGUSR1, myfunc);
        p = (char *)shmat(shmid, NULL, 0);
        if (p == NULL)
        {
            printf("shmat fail\n");
            return -1;
            /* code */
        }
        printf("C2pid:%d\n", pid);
        printf("child process shmat sucess\n");
        while (1)
        {
            pause(); //  等待父进程发信号，准备读取共享内存
            // 子进程开始读取共享内存数据，并发送信号给父进程读取完毕
            printf("child process read share memory data:%s\n", p);
            printf("getppid:%d\n", getppid());
            kill(getppid(), SIGUSR2);
            printf("getppid:%d\n", getppid());
            printf("C3pid:%d\n", pid);
            /* code */
        }
    }
    shmdt(p);                   // 共享内存脱离
    shmctl(shmid, IPC_RMID, 0); // 删除共享内存
    return 0;
}