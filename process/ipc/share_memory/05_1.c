/**1.server进程使用ftok生成一个key值，利用这个key值创建一个共享内存
 * 2.通过标准输入，向共享内存写入字符串
 * 3.server进程调用发送信号函数通知client进程
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <signal.h>
#include <string.h>

struct mybuf
{
    int pid;
    char buf[128];
    /* data */
};

void myfunc(int signal)
{
    return;
}

int main(void)
{
    int shmid;
    int key;
    struct mybuf *p;
    int pid;

    key = ftok("./a.c", 'a');
    if (key < 0)
    {
        printf("ftok creat key fail\n");
        return -1;
        /* code */
    }
    printf("ftok creat key sucess\n");
    shmid = shmget(key, 128, IPC_CREAT | 0777);
    if (shmid < 0)
    {
        printf("shmid share memory fail\n");
        return -1;
        /* code */
    }
    printf("shmid share memory sucess,shmid=%d\n", shmid);
    signal(SIGUSR2, myfunc);
    p = (struct mybuf *)shmat(shmid, NULL, 0);
    if (p == NULL)
    {
        printf("shmat fail\n");
        return -1;
        /* code */
    }
    printf("parent process shmat sucess\n");
    p->pid = getpid(); //将server进程的pid写到共享内存
    pause();           //等待client读取到server pid号
    pid = p->pid;      //  获取client的进程号

    while (1)
    {
        // 写共享内存
        printf("parent process begin write data\n");
        fgets(p->buf, 128, stdin);
        kill(pid, SIGUSR1);// 向client发送信号通知client读取共享内存数据
        pause();           // 等待client读取完共享内存数据
        /* code */
    }
    // 在用户空间删除共享空间内存的地址
    shmdt(p);
    shmctl(shmid,IPC_RMID,NULL);
    
    return 0;
}