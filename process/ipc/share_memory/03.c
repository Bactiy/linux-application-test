//  通过向共享内存写数据，并读出来，显示，脱离共享内存，删除这片共享内存
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <signal.h>

int main(void)
{
    int shmid;
    int key;
    char *p;

    key = ftok("./a.c", 'a');
    if (key < 0)
    {
        printf("ftok create key fail\n");
        return -1;
    }
    printf("ftok creat key=0x%x\n", key);
    shmid = shmget(key, 128, IPC_CREAT | 0777);
    if (shmid < 0)
    {
        printf("creat share memory fail\n");
        return -1;
    }
    printf("creat share memory ok,shmid=%d\n", shmid);

    // void *shmat(int shmid,const void *shmaddr,int shmflg);
    // shmat返回值是该段所连接的实际地址
    p = (char *)shmat(shmid, NULL, 0);
    if (p == NULL)
    {
        printf("shmat fail\n");
        return -1;
        /* code */
    }
    printf("shmat sucess\n");
    fgets(p, 10, stdin);
    // 读取共享内存数据
    printf("read share memory data:%s\n", p);
    /*  本函数调用并不删除所指定的共享内存区，
        而只是将先前用shmat函数连接(attach)好的共享内存脱离(detach)目前的进程*/
    shmdt(p);
    //  再执行获取p的值 就会报错 > Segmentation fault (core dumped)
    // printf("read share memory data:%s\n",p);

    //共享内存管理 	int shmctl(int shmid, int cmd, struct shmid_ds *buf)
    /*
        shmid:共享内存标识符 
        cmd:IPC_SET：改变共享内存的状态，把buf所指的shmid_ds结构中的uid、gid、mode复制到共享内存的shmid_ds结构内
            IPC_STAT：得到共享内存的状态，把共享内存的shmid_ds结构复制到buf中
            IPC_RMID：删除这片共享内存
        buf:共享内存管理结构体。具体说明参见共享内存内核结构定义部分
    */
    shmctl(shmid, IPC_RMID, NULL);
    system("ipcs -m");
    return 0;
    // 参考链接：https://blog.csdn.net/guoping16/article/details/6584058
}
