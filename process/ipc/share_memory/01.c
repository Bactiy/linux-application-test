//  仅仅使用IPC_PRIVATE创建共享内存

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <signal.h>

int main(void)
{
    int shmid;
    /*
        int shmget(key_t   key, size_t   size, int   flag);
        key: 标识符的规则
        size:共享存储段的字节数
        flag:读写的权限
    */
    shmid = shmget(IPC_PRIVATE, 128, 0777);
    if (shmid<0)
    {
        printf("creat share memory fail!\n");
        return -1;
        /* code */
    }
    printf("creat share memory sucess,shmid=%d\n",shmid);
    return 0;
    
}