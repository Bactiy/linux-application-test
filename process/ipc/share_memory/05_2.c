/**1.client进程使用ftok生成一个key值，利用这个key值创建一个共享内存
 * 2.client进程收到server进程发送过来的信号之后，开始读取共享内存
 * 子进程读取完共享内存数据之后，发送信号通知父进程读取完成
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <signal.h>
#include <string.h>

struct mybuf
{
    int pid;
    char buf[124];
    /* data */
};
void myfunc(int signal)
{
    return;
}

int main(void)
{
    int shmid;
    int key;
    struct mybuf *p;
    int pid;

    key = ftok("./a.c", 'a');
    if (key < 0)
    {
        printf("ftok creat key fail\n");
        return -1;
        /* code */
    }
    printf("ftok creat key sucess\n");
    shmid = shmget(key, 128, IPC_CREAT | 0777);
    if (shmid < 0)
    {
        printf("creat share memory fail\n");
        return -1;
        /* code */
    }
    printf("creat share memory sucess,shmid=%d\n", shmid);

    signal(SIGUSR1, myfunc);
    p = (struct mybuf *)shmat(shmid, NULL, 0);
    if (p == NULL)
    {
        printf("shmat fail\n");
        return -1;
        /* code */
    }
    printf("child process shmat sucess\n");

    pid = p->pid;
    p->pid = getpid();
    kill(pid, SIGUSR2); //  告诉server进程去读数据

    while (1)
    {
        pause();
        printf("child process read data:%s\n", p->buf);
        kill(pid, SIGUSR2);
        /* code */
    }
    // 在用户空间删除共享内存的地址
    shmdt(p);
    shmctl(shmid, IPC_RMID, NULL);

    return 0;
}
