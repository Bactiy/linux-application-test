//  使用ftok函数生成key创建共享内存
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <signal.h>

int main(void)
{
    int shmid;
    int key;

    key = ftok("./a.c", 'a');
    if (key < 0)
    {
        printf("ftok create key fail\n");
        return -1;
        /* code */
    }
    printf("ftok creat key=0x%x\n", key);
    shmid = shmget(key, 128, IPC_CREAT | 0777);
    if (shmid < 0)
    {
        printf("creat share memory fail\n");
        return -1;
    }
    printf("creat share memory ok,shmid=%d\n", shmid);
    return 0;
}
