// 向消息队列写数据
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/msg.h>
#include <sys/shm.h>
#include <signal.h>
#include <string.h>
struct msg_buf
{
    long type;  //消息类型
    char data[127]; //消息正文
    char ID[4];
    /* data */
};


int main(void)
{
    int msgid, readret, key;
    struct msg_buf send_buf;
    key = ftok("./a.c", 'a');
    if (key < 0)
    {
        printf("ftok creat key fail\n");
        return -1;
        /* code */
    }
    printf("ftok creat key sucess\n");
    msgid = msgget(key, IPC_CREAT | 0777);
    if (msgid<0)
    {
        printf("creat msg queue fail\n");
        return -1;
        /* code */
    }
    printf("creat msg queue msgid=%d\n",msgid);
    //  写数据到消息队列
    send_buf.type = 100;
    while (1)
    {
        // 清空数据区
        memset(send_buf.data,0,124);
        printf("please input msg:");
        fgets(send_buf.data,124,stdin);
        // 现在写数据到消息队列
        msgsnd(msgid,(void *)&send_buf,strlen(send_buf.data),0);
        /* code */
    }
    
    return 0;
}