// 从消息队列读数据

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/msg.h>
#include <sys/shm.h>
#include <signal.h>
#include <string.h>

struct read_msg
{
    long type;
    char data[127];
    char ID[4];
    /* data */
};

int main(void)
{
    int msgid, key;
    struct read_msg read_buf;
    key = ftok("./a.c", 'a');
    if (key < 0)
    {
        printf("ftok creat key fail\n");
        return -1;
        /* code */
    }
    msgid = msgget(key, IPC_CREAT | 0777);
    if (msgid<0)
    {
        printf("creat msg queue fail\n");
        return -1;
        /* code */
    }
    printf("creat msg queue sucess,msgid=%d\n",msgid);
    while (1)
    {
        memset(read_buf.data,0,124);
        // 开始从消息队列读取数据
        msgrcv(msgid,(void *)&read_buf,124,100,0);
        printf("recv data from msg queue:%s\n",read_buf.data);
        /* code */
    }
    
    

    return 0;
}