// 用信号灯实现线程同步
/**①定义一个信号灯
 * ②初始化
 * ③PV操作
 */
#include <stdio.h>
#include <stdlib.h>
#include "pthread.h"
#include "semaphore.h"
#include <sys/ipc.h>
#include <sys/sem.h>

int semid;

union semun
{
    int val;
    struct semid_ds *buf;
    unsigned short *array;
    struct seminfo *__buf;
    /* data */
};

union semun mysemun;
struct sembuf mysembuf;
void *fun(void *var) // child thread
{
    // p
    mysembuf.sem_op = -1; // p操作设置成-1
    semop(semid, &mysembuf, 1);
    for (int i = 0; i < 10; i++)
    {
        usleep(100);
        printf("this is fun j = %d\n", i);
        /* code */
    }
}

int main(void)
{
    char str[] = "hello linux\n";
    pthread_t tid;
    int ret;
    semid = semget(IPC_PRIVATE, 3, 0777);   //定义一个信号灯集
    if (semid < 0)
    {
        printf("creat semid fail\n");
        return -1;
        /* code */
    }
    printf("creat semid is %d\n", semid);
    mysemun.val = 0;
    mysembuf.sem_num = 0;
    mysembuf.sem_flg = 0;              //  0：阻塞操作
    semctl(semid, 0, SETVAL, mysemun); //信号灯初始化
    ret = pthread_create(&tid, NULL, fun, (void *)str);
    if (ret < 0)
    {
        printf("creat thread fail\n");
        return -1;
        /* code */
    }
    for (int i = 0; i < 10; i++)
    {
        usleep(100);
        printf("this is main i=%d\n", i);
        /* code */
    }
    // v
    mysembuf.sem_op = 1; // v操作设置成1
    semop(semid, &mysembuf, 1);
    while (1)
    {
        /* code */
    }
    return 0;
}