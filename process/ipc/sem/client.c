//信号灯 client
#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/sem.h>

int semid;

union semun
{
    int val;
    struct semid_ds *buf;
    unsigned short *array;
    struct seminfo *__buf;
    /* data */
};

union semun mysemun;
struct sembuf mysembuf;

int main(void)
{
    int key;
    key = ftok("./a.c", 'a');
    if (key < 0)
    {
        printf("creat key fail\n");
        return -1;
        /* code */
    }
    printf("creat key sucess\n");
    semid = semget(key, 3, IPC_CREAT | 0777); //定义一个信号灯集
    if (semid < 0)
    {
        printf("creat semid fail\n");
        return -1;
        /* code */
    }
    printf("creat semid is %d\n", semid);
    // 初始化信号灯
    mysemun.val = 0;
    mysembuf.sem_num = 0;
    mysembuf.sem_flg = 0;              //  0：阻塞操作
    semctl(semid, 0, SETVAL, mysemun); //信号灯初始化
    // p
    mysembuf.sem_op = -1; // p操作设置成-1
    semop(semid, &mysembuf, 1);
    for (int i = 0; i < 10; i++)
    {
        usleep(100);
        printf("this is client i=%d\n", i);
        /* code */
    }
    
    while (1)
    {
        /* code */
    }
    return 0;
}