// 用信号量实现线程同步
/**①定义一个信号量
 * ②初始化
 * ③PV操作
 */
#include <stdio.h>
#include <stdlib.h>
#include "pthread.h"
#include "semaphore.h"

//定义一个信号量
sem_t sem;
void *fun(void *var) // child thread
{
    // p
    sem_wait(&sem);
    for (int i = 0; i < 10; i++)
    {
        usleep(100);
        printf("this is fun j = %d\n", i);
        /* code */
    }
}

int main(void)
{
    char str[] = "hello linux\n";
    pthread_t tid;
    int ret;
    sem_init(&sem, 0, 0);
    ret = pthread_create(&tid, NULL, fun, (void *)str);
    if (ret < 0)
    {
        printf("creat thread fail\n");
        return -1;
        /* code */
    }
    for (int i = 0; i < 10; i++)
    {
        usleep(100);
        printf("this is main i=%d\n", i);
        /* code */
    }
    // v
    sem_post(&sem);
    while (1)
    {
        /* code */
    }
    return 0;
}