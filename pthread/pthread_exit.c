#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <error.h>
#include <string.h>
#include <stdlib.h>
void *thread_fun(void *args)
{
    if (strcmp("1", (char *)args) == 0)
    {
        printf("new thread return\n");
        return (void *)1;
        /* code */
    }
    if (strcmp("2", (char *)args) == 0)
    {
        printf("new thread pthread_exit\n");
        pthread_exit((void *)2);
        /* code */
    }
    if (strcmp("3", (char *)args) == 0)
    {
        printf("new thread exit\n");
        exit(3);
        /* code */
    }
}

int main(int argc, char *argv[])
{
    int err;
    pthread_t tid;
    err = pthread_create(&tid, NULL, thread_fun, (void *)argv[1]);
    if (err != 0)
    {
        printf("creat thread fail\n");
        return 0;
        /* code */
    }
    sleep(1);
    printf("main func\n");
    return 0;
}