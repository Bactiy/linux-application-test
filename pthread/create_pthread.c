#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <error.h>

void *fun(void *arge)
{
    printf("pthread new = %lu\n",(unsigned int)pthread_self());
}

int main(void)
{
    pthread_t tid1;
    int ret = pthread_create(&tid1,NULL,fun,NULL);
    if (ret!=0)
    {
        perror("pthread error\n");
        return -1;
        /* code */
    }
    printf("tid_main=%lu tid_new=%lu\n",(unsigned int)pthread_self(),tid1); // tid1标识线程创建成功后指向的空间
    sleep(1);//为了等会儿子线程 要不主线程一执行完就结束了
    
    return 0;
}