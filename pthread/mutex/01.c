/**多线程临界资源访问
 * 多个线程操作公共资源，如果是全局变量，会出现“矛盾”的现象
 * 例如：线程1想要变量自增，线程2想要变量自减
 */
#define _GNU_SOURCE
#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>

int num = 0;
void *func()
{
    while (num < 3)
    {
        num++;
        printf("%s num = %d\n", __FUNCTION__, num);
        sleep(1);
        /* code */
    }
    pthread_exit(NULL);
}
void *func1()
{
    while (num > -3)
    {
        num--;
        printf("%s num = %d\n", __FUNCTION__, num);
        sleep(1);
        /* code */
    }
    pthread_exit(NULL);
}

int main()
{
    pthread_t tid1, tid2;
    int ret = pthread_create(&tid1, NULL, func, NULL);
    if (ret != 0)
    {
        perror("creat thread fail\n");
        return -1;
        /* code */
    }
    ret = pthread_create(&tid2, NULL, func1, NULL);
    if (ret != 0)
    {
        perror("creat thread fail\n");
        return -1;
        /* code */
    }
    pthread_join(tid1, NULL);
    pthread_join(tid2, NULL);
    return 0;
}