/**线程信号量控制测试
 * 
*/
#define _GNU_SOURCE
#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>

void *func1()
{
    printf("%s,come!\n",__FUNCTION__);
    pthread_exit(NULL);
}
void *func2()
{
    printf("%s,come!\n",__FUNCTION__);
    pthread_exit(NULL);
}
void *func3()
{
    printf("%s,come!\n",__FUNCTION__);
    pthread_exit(NULL);
}

int main()
{
    pthread_t tid1,tid2,tid3;
    int ret = pthread_create(&tid1,NULL,func1,NULL);
    if (ret!=0)
    {
        perror("creat thread fail\n");
        return -1;
        /* code */
    }
    ret = pthread_create(&tid2,NULL,func2,NULL);
    if (ret!=0)
    {
        perror("creat thread fail\n");
        return -1;
        /* code */
    }
    ret = pthread_create(&tid3,NULL,func3,NULL);
    if (ret!=0)
    {
        perror("creat thread fail\n");
        return -1;
        /* code */
    }
    pthread_join(tid1,NULL);
    pthread_join(tid2,NULL);
    pthread_join(tid3,NULL);
    return 0;
}

/*      实验现象：（会出现函数顺序混乱）
        func1,come!
        func3,come!
        func2,come!
*/