/**线程信号量控制测试
 *
 */
#define _GNU_SOURCE
#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <semaphore.h>

sem_t sem_1, sem_2, sem_3;

void *func1()
{
    sem_wait(&sem_1);
    printf("%s,come!\n", __FUNCTION__);
    sem_post(&sem_2);
    pthread_exit(NULL);
}
void *func2()
{
    sem_wait(&sem_2);
    printf("%s,come!\n", __FUNCTION__);
    sem_post(&sem_3);
    pthread_exit(NULL);
}
void *func3()
{
    sem_wait(&sem_3);
    printf("%s,come!\n", __FUNCTION__);
    sem_post(&sem_1);
    pthread_exit(NULL);
}

int main()
{
    pthread_t tid1, tid2, tid3;
    /*
        int sem_init(sem_t *sem,int pshared,unsigned int value);
        参数1：sem_t类型地址
        参数2：0表示线程控制，否则为进程控制
        参数3：信号量的初始值
                0：阻塞
                1：运行
    */
    int rsem = sem_init(&sem_1, 0, 1);
    if (rsem < 0)
    {
        perror("sem_init fail\n");
        return -1;
        /* code */
    }
    rsem = sem_init(&sem_2, 0, 0);
    if (rsem < 0)
    {
        perror("sem_init fail\n");
        return -1;
        /* code */
    }

    rsem = sem_init(&sem_3, 0, 0);
    if (rsem < 0)
    {
        perror("sem_init fail\n");
        return -1;
        /* code */
    }

    int ret = pthread_create(&tid1, NULL, func1, NULL);
    if (ret != 0)
    {
        perror("creat thread fail\n");
        return -1;
        /* code */
    }
    ret = pthread_create(&tid2, NULL, func2, NULL);
    if (ret != 0)
    {
        perror("creat thread fail\n");
        return -1;
        /* code */
    }
    ret = pthread_create(&tid3, NULL, func3, NULL);
    if (ret != 0)
    {
        perror("creat thread fail\n");
        return -1;
        /* code */
    }
    /*回收线程资源*/
    pthread_join(tid1, NULL);
    pthread_join(tid2, NULL);
    pthread_join(tid3, NULL);
    /*销毁信号量*/
    sem_destroy(&sem_1);
    sem_destroy(&sem_2);
    sem_destroy(&sem_3);
    return 0;
}

/*      实验现象：
 */