#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <error.h>
#include <string.h>
#include <stdlib.h>

void *func(void *arg)
{
    static int tmp = 0;
    tmp = *(int *)arg;
    tmp += 100;
    printf("%s,Addr:%p,val = %d\n", __FUNCTION__, &tmp, tmp);
    pthread_exit((void *)&tmp);
}

int main()
{
    pthread_t tid;
    int a = 50;
    void *Tmp = NULL;
    int ret = pthread_create(&tid, NULL, func, (void *)&a);
    if (ret != 0)
    {
        perror("pthread creat fail\n");
        return -1;
        /* code */
    }
    pthread_join(tid, &Tmp);
    printf("%s,Addr:%p,val = %d\n", __FUNCTION__, Tmp, *(int *)Tmp);
    return 0;
}

/*      运行结果：
        func,Addr:0x560d3c367014,val = 150
        main,Addr:0x560d3c367014,val = 150
*/