#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <error.h>

struct student
{
    int age;
    char name[20];
    /* data */
};


void *fun(void *stu)
{
    printf("pthread new = %lu\n",(unsigned int)pthread_self());
    printf("name:%s\tage:%d\n",((struct student *)stu)->name,((struct student *)stu)->age);
}

int main(void)
{
    struct student stu;
    stu.age = 10;
    memcpy(stu.name,"liming",6);

    pthread_t tid1;
    int ret = pthread_create(&tid1,NULL,fun,(void *)&stu);
    if (ret!=0)
    {
        perror("pthread error\n");
        return -1;
        /* code */
    }
    printf("tid_main=%lu tid_new=%lu\n",(unsigned int)pthread_self(),tid1); // tid1标识线程创建成功后指向的空间
    sleep(1);//为了等会儿子线程 要不主线程一执行完就结束了
    
    return 0;
}