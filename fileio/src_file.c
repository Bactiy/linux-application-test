#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
/*  打开一个已经存在的文件（yet_file.txt）--只读方式
*   打开一个新建文件（dest_file）--只写方式 文件所属着拥有 **读、写、执行** 同组用户与其他用户拥有 **读**
*   从yet_file文件偏移头部的500个字节位置开始读取1Kbyte数据，
    然后将读出来的数据写入到dest_file文件中，从文件开头写入1Kbyte字节大小，
    操作完成之后使用close关闭所有文件
*/
#define FILE_EXIST
// #define COPY
// #define LSEEK
int main(int argc, char **argv)
{
#ifdef COPY
    int fd_old, fd_new;
    char buf[1024];
    int len;

    int curr_local;
    /* 1. 判断参数 */
    if (argc != 3)
    {
        printf("Usage: %s <old-file> <new-file>\n", argv[0]);
        return -1;
    }
    /* 2. 打开老文件 以只读方式*/
    fd_old = open(argv[1], O_RDONLY);
    if (fd_old == -1)
    {
        printf("can not open file %s\n", argv[1]);
        return -1;
    }

    fd_new = open(argv[2], O_WRONLY | O_CREAT, S_IRWXU | S_IRGRP | S_IROTH);
    if (fd_new == -1)
    {
        printf("can not creat file %s\n", argv[2]);
        return -1;
    }
    /*
        lseek
        第一个参数：要偏移的文件名称
        第二个参数：偏移几个字节
        第三个参数：将读写位置指向文件头后再增加几个位移量。
        参考链接：https://blog.csdn.net/sinat_36629696/article/details/80001104
    */
    curr_local = lseek(fd_old, 2, SEEK_SET); //

    if (read(fd_old, buf, 3) > 0)
    {
        write(fd_new, buf, 3);
    }
    // while ((len = read(fd_old, buf, 3)) > 0)
    // {
    // 	if (write(fd_new, buf, len) != len)
    // 	{
    // 		printf("can not write %s\n", argv[2]);
    // 		return -1;
    // 	}
    // }

    close(fd_old);
    close(fd_new);
#endif // COPY
#ifdef FILE_EXIST
    int fd_exist;
    /* 1. 判断参数 */
    if (argc != 2)
    {
        printf("Usage: %s <file_name>\n", argv[0]);
        return -1;
    }

    fd_exist = open(argv[1], O_RDONLY);
    if (fd_exist == -1)
    {
        printf("[ %s ]does not exist\n", argv[1]);
        return -1;
    }
#endif // FILE_EXIST

#ifdef LSEEK
    /*打开一个已经存在的文件（例如 yet_file），通过 lseek 函数计算该文件的大小，并打印出来。*/
    int fd_name, fd_size;
    fd_name = open(argv[1], O_RDONLY);
    if (fd_name == -1)
    {
        printf("can not open file %s\n", argv[1]);
        return -1;
    }
    fd_size = lseek(fd_name, 0, SEEK_END); 
    printf("[ %s ]byte :%d\n", fd_size);
#endif // LSEEK
    return 0;
}